(function() {
  'use strict';

  angular
    .module('starter')
    .factory('OrderPlaceService', OrderPlaceService);

    function OrderPlaceService($http) {
        var service = {};
        var orderPlaceSelected;
        var ordersPlace = 'undefined';

        
        service.getAllordersPlaced = function (callback) {
              $http.get(urlws + '/allActiveOrders/' )
              .success(function (response) {
                callback(response.data);
                ordersPlace = response.data;
              }).
              error(function(data) {
                console.log('error get all ordersPlace');
              });
        };

        service.setOrder = function (order) {
          orderPlaceSelected = order;
        }

        service.getOrderSelected = function (callback) {
          callback(orderPlaceSelected);
        }

        return service;
    }
})();
