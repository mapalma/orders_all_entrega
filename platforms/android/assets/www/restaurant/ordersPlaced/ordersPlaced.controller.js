(function() {
  'use strict';

  angular
    .module('starter')
    .controller('OrdersController', OrdersController);

  /** @ngInject */
  function OrdersController(OrderPlaceService,MapService) {
    var vm = this;
    vm.setOrder = setOrder;


    function getOrdersPlaced() {
    	OrderPlaceService.getAllordersPlaced(function(response){
    		vm.ordersPlaced = response;
        vm.productsOrders = angular.fromJson(vm.ordersPlaced.json);
    	});
    }


    function setOrder(order) {
      OrderPlaceService.setOrder(order);
      console.log('error get all ordersPlace xxxxxxxxxxxx',order.coordenadas);
      MapService.setPosition(order.coordenadas);
      loadMap();
    }

    function loadMap() {
      console.log("cargar mapa");
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDzzRp2LaRXcRUEBO2T1uIfLb7tOk58JAM';
      document.body.appendChild(script);
  }


	   getOrdersPlaced();

  }
})();
