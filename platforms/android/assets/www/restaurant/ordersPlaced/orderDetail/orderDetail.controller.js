(function() {
  'use strict';

  angular
    .module('starter')
    .controller('OrdersDetailController', OrdersDetailController);

  /** @ngInject */
  function OrdersDetailController(OrderPlaceService,$state,StorageService,$timeout,SenderService) {
      var vm = this;
      vm.saveByEstado=saveByEstado;

    function getOrderSelected() {
      OrderPlaceService.getOrderSelected(function (order) {
        vm.orderSelected = order;
        console.log("las ordenes seleccionadas son " , vm.orderSelected);
        vm.productsOrders = angular.fromJson(vm.orderSelected.json);
        console.log("la seccion dos es " ,vm.productsOrders );
        calculateSubTotal();
      });
    }

    function calculateSubTotal() {
      vm.subtotal = 0;
      angular.forEach(vm.productsOrders, function(value, key) {
        vm.subtotal += (value.cantidad * value.valor);
      });
      calculateTotal();
    }

    function calculateTotal() {
      console.log("subtotal " , vm.subtotal);
      console.log("costo envio " ,  vm.orderSelected.costo_envio);
      vm.total = (parseInt(vm.subtotal) + parseInt(vm.orderSelected.costo_envio));
    }
      function saveByEstado () {


          var data = {
              estado : 'ENTREGADO',
              idpedido:vm.orderSelected.idpedido_fast

          };

          console.log("el usuario es " , data);
          SenderService.sendEstado(data, function(response) {
              console.log("el estado es ", response);
              $state.go('ordersPlaced');
          });

      };
    //load data
	   getOrderSelected();
  }
})();
