angular.module('generalController', [])
    .controller('SupermercadoCtrl', function ($scope, 
                                              $http,
                                              $translate, 
                                              $state, 
                                              $location, 
                                              $ionicHistory, 
                                              StorageService,
                                              listasService, 
                                              $ionicSideMenuDelegate,
                                              $ionicLoading) {

  $scope.getAllSupermercados    = getAllSupermercados;
  $scope.cargarSupermercado     = cargarSupermercado;
  $scope.recargarSupermercados  = recargarSupermercados;

	function getAllSupermercados () {
    //conexionintersuper(); // verificar la conexion a internet
    $scope.conexion_internet_super = true;

    if ($scope.conexion_internet_super) {
      showLoading($ionicLoading);
      listasService.doSupermercados().then(function(respuesta) {
        $scope.listasupermercados = respuesta.data;
        var result = respuesta.data;
        hideLoaging($ionicLoading);
      });
    }  
  }

  function recargarSupermercados() {
    getAllSupermercados();
    conexionintersuper();
    $scope.$broadcast('scroll.refreshComplete');
  };

  function conexionintersuper() {
    var networkState = navigator.connection.type;
    if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
        $scope.conexion_internet_super = false;
    } else {
        $scope.conexion_internet_super = true;
    }
  }

  function cargarSupermercado(bd_name, _promocion, _ubicacion, _listas, _id_paypal, _pagar_entrega) {
    if (!_pagar_entrega) {
      _pagar_entrega = 0.0
    }
    
    StorageService.removeAll();
    StorageService.add({bd : bd_name,
                        promocion: _promocion,
                        ubicacion: _ubicacion,
                        listas: _listas,
                        id_paypal: _id_paypal,
                        pagar_entrega: _pagar_entrega});
     //cargar id para paypal
    if (_id_paypal) {
      id_paypal = _id_paypal;
      lenguaje = $translate.use();
      shopPaypal.initialize(); 
    }
  }

})

showLoading = function($ionicLoading) {
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });
};

hideLoaging = function ($ionicLoading) {
  $ionicLoading.hide();
};
