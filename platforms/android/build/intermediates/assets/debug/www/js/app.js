// Ionic Starter App

var listaResultado = [];
litasMetodos = function (listaLLegada) {
    listaResultado = listaLLegada;
};

var db = null;
var inciarListas = [];
var app = angular.module('starter', ['ionic',
                                         'ngCordova',
                                         'ngStorage',
                                         'starter.controllers',
                                         'loginController',
                                         'generalController',
                                         'pascalprecht.translate'])
    /*inicia la app*/
    .run(function ($ionicPlatform,
                   $cordovaSQLite,
                   $rootScope,
                   StorageService,
                   $location,
                   listasService) {

        $ionicPlatform.ready(function () {

            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }

            /*tamanio de la base de datos*/
            var dbSize = 5 * 1024 * 1024; // 5MB

            if (window.cordova) {
               // db = $cordovaSQLite.openDB({name: "shopbd.db"}); //device
               db = $cordovaSQLite.openDB( 'shopbd.db', 1 );
            } else {
                /*creamos bd*/
                //db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser
                db = window.openDatabase("my.db", "1.0", "Córdoba Demo", 200.000); //para chrome
            }
            /*creamos las tablas*/
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS listasrk(ID INTEGER PRIMARY KEY ASC, nombre VARCHAR, precio FLOAT, bd_super VARCHAR, fecha DATE, id_lista_super INTEGER)");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS productosrk(ID INTEGER PRIMARY KEY ASC, nombre VARCHAR, codigo INTEGER, precio FLOAT, id_lista INTEGER,  imagen VARCHAR, cantidad INTEGER , precioTotal FLOAT, ubicacion_id VARCHAR , estado BOOLEAN , comprado BOOLEAN)");

            var arreglo = StorageService.getUser();
            //var usuario = arreglo[0];

            if(arreglo.length > 0) {
               // $location.path('/restaurants');
                //cargar los supermercados
                listasService.doSupermercados().then(function(respuesta) {

                });
            }else {

                //$location.path('/login');
                //$state.go('login');
            }
        });
    })
        /*configuracion*/
        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider) {
            $ionicConfigProvider.views.maxCache(0); //en caso de cambiar valores a la vista que se actulicen automaticamente

            //para el idioma
            for (lang in translations) {
                $translateProvider.translations(lang, translations[lang]);
            }

            $translateProvider.preferredLanguage('es');

            $stateProvider
                    // navegation

                    //login
                    .state('login', {
                        url: '/login',
                        templateUrl: 'views/login.html',
                        controller: 'loginCtrl'
                    })

                    .state('recuperarclave', {
                        url: '/recuperarclave',
                        templateUrl: 'views/recuperar-clave.html',
                        controller: 'recuperarCtrl'
                    })
                    //user
                    .state('perfil', {
                        url: '/perfil',
                        templateUrl: 'templates/perfil.html',
                        controller: 'PerfilCtrl'
                    })

                    //====================================//
                    //         RESTAURANTS                //
                    //====================================//


                    .state('ordersPlaced', {
                        url: '/ordersPlaced',
                        templateUrl: 'restaurant/ordersPlaced/ordersPlaced.view.html',
                        controller : 'OrdersController as vm'
                    })
                    .state('map', {
                        url: '/map',
                        templateUrl: 'restaurant/ordersPlaced/mapa/mapapedido.html',
                        controller : 'MapController as vm'
                    })
                    .state('orderDetail', {
                        url: '/orderDetail',
                        templateUrl: 'restaurant/ordersPlaced/orderDetail/orderDetail.view.html',
                        controller : 'OrdersDetailController as vm'
                    })

            // al iniciar la app se cargara la url bienvenido
            $urlRouterProvider.otherwise('/login');

        })
        // create a new factory
        .factory ('StorageService', function ($localStorage) {
            //local store para el usuario
            $localStorage.user = [];
            var _saveUser = function (user) {
              $localStorage.user.push(user);
            }
            var _getUser = function () {
              return $localStorage.user;
            };
            var _deleteUser = function () {
              return $localStorage.user =[];
            };

            $localStorage = $localStorage.$default({
                things: []
            });

            var _getAll = function () {
              return $localStorage.things;
            };

            var _getBd = function () {
              return $localStorage.things[0].bd;
            };
            var _getPaypal = function () {
              return $localStorage.things[0].id_paypal;
            };
            var _pagar_entrega = function () {
              return $localStorage.things[0].pagar_entrega;
            };

            var _add = function (thing) {
              $localStorage.things.push(thing);
            }

            var _remove = function (thing) {
              $localStorage.things.splice($localStorage.things.indexOf(thing), 1);
            }
            var _removeAll = function (thing) {
              $localStorage.things = [];
            }

            return {
                getAll      : _getAll,
                add         : _add,
                remove      : _remove,
                removeAll   : _removeAll,
                getBd       : _getBd,
                saveUser    : _saveUser,
                getUser     : _getUser,
                deleteUSer  : _deleteUser,
                getPaypal   : _getPaypal,
                pagar_entrega:_pagar_entrega

              };
        });
