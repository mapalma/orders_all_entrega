(function() {
  'use strict';

  angular
    .module('starter')
    .controller('RestaurantController', RestaurantController);

  /** @ngInject */
  function RestaurantController(RestaurantService, ProductService, PedidoService,  $state, $ionicHistory, $ionicPopup) {
    var vm = this;
    vm.changeTo = changeTo;
    vm.loadMenu = loadMenu;
    vm.loadTemplate = 'restaurant/restaurants/restaurant/restaurant.tpl.html';
    vm.redirectoToProduct = redirectoToProduct;
    vm.addCar = addCar;
    vm.confirmDestroyOrder = confirmDestroyOrder;

    vm.pedidos = {
        platos : [],
        promociones : [],
        ensaladas : [],
        bebidas : [],
        postres : [],
        entradas : []
    };

    function loadRestaurant() {
      RestaurantService.getRestaurantSelected(function(response) {
          vm.restaurant = response;
      });
    }

    function loadNumberProducts() {
      PedidoService.getNumbersProducts(function (response) {
        console.log("los productos agregados son ", response);
        vm.numberProducts = response;
      });
    }

    function changeTo(section) {
      if (section === 'restaurant') {
        vm.loadTemplate = 'restaurant/restaurants/restaurant/restaurant.tpl.html'
      } else if (section === 'menu') {
        vm.loadTemplate = 'restaurant/restaurants/restaurant/menu.tpl.html'
      } else if (section === 'comment') {
        vm.loadTemplate = 'restaurant/restaurants/restaurant/comments.tpl.html'
      }
    }

    function redirectoToProduct(product) {
      //load in service the product selected
      ProductService.setProduct(product);
      $state.go('detailProduct');
    }

    function addCar(product) {
      PedidoService.addProduct(product);
      console.log("producto agregago 123");
      loadNumberProducts();
    }

    function loadMenu(seccion) {
      if (seccion === 'plate') {
        console.log("la longitud es " , vm.pedidos.platos.length);
        vm.pedidos.platos = vm.pedidos.platos.length === 0 ? vm.restaurant.platos : [] ;
      } else if (seccion === 'promo') {
        vm.pedidos.promociones = vm.pedidos.promociones.length === 0 ? vm.restaurant.promociones : [] ;
      } else if (seccion === 'salad') {
        vm.pedidos.ensaladas = vm.pedidos.ensaladas.length === 0 ? vm.restaurant.ensalada : [] ;
      } else if (seccion === 'drink') {
        vm.pedidos.bebidas = vm.pedidos.bebidas.length === 0 ? vm.restaurant.bebidas : [] ;
      } else if (seccion === 'desseert') {
        vm.pedidos.postres = vm.pedidos.postres.length === 0 ? vm.restaurant.postres : [] ;
      } else if (seccion === 'entrance') {
        vm.pedidos.entradas = vm.pedidos.entradas.length === 0 ? vm.restaurant.entradas : [] ;
      }
    }

  function confirmDestroyOrder() {
      if (vm.numberProducts > 0) {
        $ionicPopup.confirm({
          title: 'Advertencia.!',
          template: 'Esta seguro que desea Salir?'
        }).then(function(res) {
          if (res) {
            $ionicHistory.goBack();
            //detroy list
            PedidoService.destroyOrder();
          }
        })
      } else {
        $ionicHistory.goBack();
      }
  }


  //loadRestaurantSelected
    loadRestaurant();
    loadNumberProducts();
  }
})();
