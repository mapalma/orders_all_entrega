var listasProductos = [];
//load variables
angular.module('starter.controllers', [])

        //controlador para el idioma
        .controller('lenguajeCtrl', function ($scope, $translate) {
            $scope.ChangeLanguage = function (lang) {
                $translate.use(lang);
            }

        })
        //controlador para seccion de ubicaciones de producto
        .controller('ubicacionCtrl', function ($scope) {
            $scope.existeInternetUbicacionGeneral = function () {

                //conecionInternet
                var networkState = navigator.connection.type;
                if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
                    $scope.conexion_internet_ubicacion_g = false;
                } else {
                    $scope.conexion_internet_ubicacion_g = true;
                }

               // $scope.conexion_internet_ubicacion_g = true;
            }

        })
        .controller('menuCtrl', function ($scope) {
            $scope.hideSubmenu = function () {
                $scope.submenu = false;
            }
            $scope.showSubmenu = function () {
                $scope.submenu = $scope.submenu === true ? false : true;
            }

        })
        .controller('ubisupermercadoCtrl', function ($scope,
                                                     $translate,
                                                     $http,
                                                     StorageService,
                                                     $ionicHistory,
                                                     $state) {

            //retoceder a una vista anterior

            $scope.irSupermercado = function(){
                $state.go('ubi-supermercado');
            }  
            $scope.myGoBack = function () {
                console.log("retroceder");
                $ionicHistory.goBack();
            };

            $scope.existeInternetSupermercado = function () {
                /*cargar script de google para la ubicacion*/
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' + 'libraries=places&'+'callback=initialize';
                document.body.appendChild(script);

                //conecionInternet
                var networkState = navigator.connection.type;

                if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
                    $scope.conexion_internet_supermercado = false;
                } else {
                    $scope.conexion_internet_supermercado = true;
                }
                //$scope.conexion_internet_supermercado = true;
            }

            $scope.getDatosSupermercado = function () {
                var bd_super = StorageService.getBd();
                var url = urlws + "/web/supermercadomovil/" + bd_super;
                $http.get(url).success(function (respuesta) {
                    //respuesta del servidor
                    if (respuesta != "")
                        $scope.datosSupermercado = respuesta[0];
                }).error(function (data, status, headers, config) {
                    $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                    console.log('-->' + $scope.token);
                });
            }

            $scope.iniciarmap = function (datosSupermercado) {
                //verificar si existen datos
                if (datosSupermercado.latitud && datosSupermercado.longitud && datosSupermercado.nombre) {
                    /*longitud y latitud el supermercado*/
                    var myLatlng = new google.maps.LatLng(datosSupermercado.latitud, datosSupermercado.longitud);
                    // var myLatlng = new google.maps.LatLng(-4.0009026, -79.2330311);
                    var mapOptions = {
                        zoom: 17,
                        center: myLatlng
                    }
                    /*dibujar el mapa en el html*/
                    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map
                    });
                    /*cargar nombre del supermecado en el puntero del mapa*/
                    var infowindow = new google.maps.InfoWindow({content: datosSupermercado.nombre});
                    infowindow.open(map, marker);
                } else {
                    /*presentar un mensaje de en caso de que la consulta no obtuvo los datos correspondientes*/
                    $translate(['errorgetInfSuper']).then(function (translations) {
                        window.plugins.toast.showShortBottom(translations.errorgetInfSuper + ". !", function (a) {
                            console.log('toast success: ' + a)
                        }, function (b) {
                            alert('toast error: ' + b)
                        });
                    });

                }
            };
        })

        //controlador seccion listas
        .controller('listasTableCtrl', function ($scope, 
                                                 $cordovaSQLite, 
                                                 $ionicPopup, 
                                                 $translate, 
                                                 $stateParams,
                                                 StorageService,
                                                 listasService) {

            //dialog cambia nombre lista
            $scope.showConfirm = function (lista) {
                $translate(['nuevonombre', 'in']).then(function (translations) {
                    var confirmPopup = $ionicPopup.confirm({
                        title: translations.nuevonombre,
                        template: "<input type='text' id='nuevoNombreLista' placeholder=" + translations.in + "  ></input>"
                    });
                    confirmPopup.then(function (res) {
                        if (res) {
                            //si ingresa texto actualizar nombre lista
                            if (document.getElementById('nuevoNombreLista').value) {
                                $scope.updateLista(lista.id, document.getElementById('nuevoNombreLista').value);
                            }
                        }
                    });
                });
            };

            //arreglo de todas las listas
            $scope.getListas = [];

            //guarda una lista en la bd
            $scope.insert = function () {
                var nombreLista = $scope.nombreNuevaLista;
                var bd_super = StorageService.getBd();
                if (nombreLista && bd_super) {
                    var fecha = new Date();
                    var datestring = ("0" + fecha.getDate()).slice(-2) + "-" + ("0"+(fecha.getMonth()+1)).slice(-2) + "-" +
                        fecha.getFullYear() + " " + ("0" + fecha.getHours()).slice(-2) + ":" + ("0" + fecha.getMinutes()).slice(-2);
                        var date = datestring;

                    var query = "INSERT INTO listasrk (nombre, precio, bd_super, fecha) VALUES (?, ?, ?, ?)";
                    $cordovaSQLite.execute(db, query, [nombreLista, 0.0, bd_super, date]).then(function (res) {
                        if ($scope.getListas.length != 0) { //si contiene valores desplazar lista
                            $scope.getListas.splice(0, 0, {"id": res.insertId, "precio": 0.0, "nombre": nombreLista, "fecha": date});
                            //presentar un mensaje de guardado correcto
                            $translate(['agregado']).then(function (translations) {
                                window.plugins.toast.showShortBottom(translations.agregado + ". !", function (a) {
                                    console.log('toast success: ' + a)
                                }, function (b) {
                                    alert('toast error: ' + b)
                                });
                            });
                        } else {
                            $scope.getListas.push({"id": res.insertId, "nombre": nombreLista , "precio": 0.0, "fecha" : date});
                        }
                    }, function (err) {
                        alert('error en ' + err);
                        console.error(err);
                    });
                    //limpiar el campo de ingreso nueva lista
                    $scope.nombreNuevaLista = "";
                }

            };
            //actualiza nombre Lista
            $scope.updateLista = function (id_lista, nuevoNombre) {
                var update = "UPDATE  listasrk SET nombre = ? WHERE ID = ? ";
                $cordovaSQLite.execute(db, update, [nuevoNombre, id_lista]).then(function (res) {
                    $scope.getAllListas();
                }, function (err) {
                    console.error(err);
                });
            };

            //elimina lista
            $scope.deleteLista = function (lista) {
                if ($scope.getListas.length === 1){
                    $scope.verListas = false;
                }
               $scope.getListas.splice($scope.getListas.indexOf(lista), 1);
                var id = lista.id;
                var querydeleteLista = "DELETE FROM listasrk WHERE id = ?";
                $cordovaSQLite.execute(db, querydeleteLista, [id]);
                //elimina productos que le pertenecen a la lista
                var querydeleteProductos = "DELETE FROM productosrk WHERE id_lista =  ?";
                $cordovaSQLite.execute(db, querydeleteProductos, [id]);
            };

            //actualiza las listas
            $scope.doRefresh = function () {
                $scope.getAllListas();
                $scope.$broadcast('scroll.refreshComplete');
            };
            //obtiene todas las listas de la bd
            $scope.getAllListas = function () {
                var listasarreglo = [];
                var namebd_super = StorageService.getBd();
                $scope.nombreSupermercadoSelect = namebd_super;
                var query = "SELECT * FROM listasrk WHERE bd_super = ? ORDER BY ID DESC";
                $cordovaSQLite.execute(db, query, [namebd_super]).then(function (res) {
                    if (res.rows.length > 0) {
                        for (var i = 0, max = res.rows.length; i < max; i++) {
                            listasarreglo.push({"id"            : res.rows.item(i).ID, 
                                                "nombre"        : res.rows.item(i).nombre, 
                                                "precio"        : res.rows.item(i).precio, 
                                                "id_lista_super": res.rows.item(i).id_lista_super,
                                                "fecha"         : res.rows.item(i).fecha});
                        }
                        //cargar listas para vista
                        $scope.getListas = listasarreglo;
                        //cargar listas para utilizacion interna
                        totalListas = listasarreglo;

                    } else {
                        console.log("No results found");
                    }
                }, function (err) {
                    console.error(err);
                });
                return null;
            }

            $scope.verListas = true;

            $scope.listasAll = function (namebd_super) {
                var listasarreglo = [];
                $scope.nombreSupermercadoSelect = namebd_super;
                var query = "SELECT * FROM listasrk";
                $cordovaSQLite.execute(db, query, []).then(function (res) {
                    console.log("la longitud del arreglo es " , res.rows.length);
                    if (res.rows.length > 0) {
                        for (var i = 0, max = res.rows.length; i < max; i++) {
                            console.log(res.rows.item(i));
                            listasarreglo.push({"id"            : res.rows.item(i).ID, 
                                                "nombre"        : res.rows.item(i).nombre, 
                                                "precio"        : res.rows.item(i).precio, 
                                                "id_lista_super": res.rows.item(i).id_lista_super,
                                                "bd_super"      : res.rows.item(i).bd_super,
                                                "fecha"         : res.rows.item(i).fecha});
                        }
                        //cargar listas para vista
                        $scope.getListas = listasarreglo;
                        //cargar listas para utilizacion interna
                        totalListas = listasarreglo;
                    } else {
                        $scope.verListas = false;
                        console.log("No results found");
                    }
                }, function (err) {
                    console.error(err);
                });
                return null;
            };

            $scope.getSuper = function(nombre_bd) {
                var supermercados = listasService.getSupermercados();
                if (supermercados) {
                   var supermercado = metodos.getSupermercado(supermercados.data, nombre_bd);
                   return supermercado;
                } else {
                    console.log(">>> " , $scope.listasupermercados);
                }
                //var supermercado = metodos.getSupermercado(listaSupermercados, nombre_bd);
                //console.log("el supermercado encontrado es " , supermercado);
                //return supermercado;
            }

            $scope.cargarListasSuper = function() {
                listasService.doSupermercados().then(function(respuesta) {
                    $scope.listasupermercados = respuesta.data;
                });
            }
        })
        //controlador seccion productos
        .controller('productosCtrl', function ($scope,
                                               $stateParams,
                                               $cordovaSQLite,
                                               $rootScope,
                                               $translate,
                                               StorageService,
                                               listasService) {
            //seccion para cargar los servicios, con los que cuenta cada supermercado
            var result = StorageService.getAll();
            $scope.servicioCompras = result[0].listas;
            
            $scope.existeInternetScan = function () {
                //conecionInternet
                /*var networkState = navigator.connection.type;

                if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
                    $scope.conexion_internet_escaner = false;
                } else {
                    $scope.conexion_internet_escaner = true;
                }*/

                $scope.conexion_internet_escaner = true;

            }

            /*colorear de azul cuando se carga el campo input para el ingreso de un nombre*/
            $scope.colorear = "calm-border";

            $scope.colorSuccess = function () {
                $scope.colorear = "calm-border";
            }

            $scope.ocultarCampo = function () {
                $scope.formVisibility = false;

            };

            $scope.ShowForm = function () {
                $scope.formVisibility = true;
                console.log($scope.formVisibility);
            };

            //refrescar el arreglo de los productos
            $scope.doRefresh = function () {
                $scope.getAllProductos();
                $scope.$broadcast('scroll.refreshComplete');
            };

            $scope.lista = metodos.getListaId($stateParams.listaId);//revisar esta lista

            //insertar productos buscados a bd
            $scope.insertProductos = function (producto) {
                var id_lista = $stateParams.listaId;
                var cantidad = 1;
                var precioTotal = producto.pvp_prod;
                var data = {"nombre "   : producto.nom_prod, 
                            "codigo"    : producto.codigo,
                            "preciovp"  : producto.pvp_prod,
                            "id_lista"  : id_lista, 
                            "imagen"    : producto.imagen, 
                            "cantidad " : cantidad, 
                            "precioT"   : precioTotal, 
                            "ubicacion" : producto.ubicacion_id};

                var query = "INSERT INTO productosrk (nombre, codigo, precio , id_lista, imagen , cantidad , precioTotal , ubicacion_id , estado , comprado) VALUES (? ,?, ?, ? , ? , ?, ? , ? , ? , ?)";
                $cordovaSQLite.execute(db, query, [producto.nom_prod, producto.codigo, producto.pvp_prod, id_lista, producto.imagen, cantidad, precioTotal, producto.ubicacion_id, true, false]).then(function (res) {
                    //para la seccion de busqueda .. en caso de ser agregado que se elimine de la vista
                    $scope.productosEncontrados.splice($scope.productosEncontrados.indexOf(producto), 1);
                    //presentar un mensaje de producto agregado
                    $translate(['productoadd']).then(function (translations) {
                        window.plugins.toast.showShortBottom(translations.productoadd + ". !", function (a) {
                            console.log('toast success: ' + a)
                        }, function (b) {
                            alert('toast error: ' + b)
                        });
                    });
                }, function (err) {
                    console.error(err);
                });
            };
         
            //insertar productos escaneados a bd
            $scope.insertProductosCodebar = function (producto) {

                var id_lista = $stateParams.listaId;
                var cantidad = 1;
                var precioTotal = producto.pvp_prod;
                if (!precioTotal) {
                    precioTotal = 0.0;
                }

                var query = "INSERT INTO productosrk (nombre, codigo, precio , id_lista, imagen , cantidad , precioTotal , ubicacion_id , estado , comprado) VALUES (? ,?, ?, ? , ? , ?, ? , ? , ? , ?)";
                $cordovaSQLite.execute(db, query, [producto.nom_prod, producto.codigo, producto.pvp_prod, id_lista, producto.imagen, cantidad, precioTotal, producto.ubicacion_id, true, false]).then(function (res) {

                    //desplazar la lista de la vista
                    $scope.resultadoproducto.splice($scope.resultadoproducto.indexOf(producto), 1);
                    /*mensaje de producto agregado*/
                    $translate(['agregado']).then(function (translations) {
                        window.plugins.toast.showShortBottom(translations.agregado + ". !", function (a) {
                            console.log('toast success: ' + a)
                        }, function (b) {
                            alert('toast error: ' + b)
                        });
                    });

                }, function (err) {
                    console.error(err);
                });
            };
            //valida un numero flotante
            validaFloat = function (numero) {
                if (!/^([0-9])*[.]?[0-9]*$/.test(numero)) {
                    return false;
                } else {
                    return true;
                }
            };
            //actualiza producto en bd
            $scope.updateProducto = function (producto) {
                if (validaFloat(producto.precio)) {
                    var precioTotal = producto.precio * producto.cantidad;
                    var update = "UPDATE  productosrk SET nombre = ? , precio= ? , cantidad =? , precioTotal = ?  WHERE ID = ? ";
                    $cordovaSQLite.execute(db, update, [producto.nombre, producto.precio, producto.cantidad, precioTotal, producto.id]).then(function (res) {
                        $scope.doRefresh();
                    }, function (err) {
                        console.error(err);
                    });
                    $scope.getAllProductos();
                }
            };
            //actualiza producto en bd
            $scope.updateProductoComprado = function (producto, id) {

                var insertar;
                if (producto.comprado == 'false') {
                    insertar = true;
                    producto.comprado = 'true';
                } else {
                    insertar = false;
                    producto.comprado = 'false';
                }

                /*actualizar un producto*/
                var update = "UPDATE  productosrk SET comprado = ?  WHERE ID = ? ";
                $cordovaSQLite.execute(db, update, [insertar, producto.id]).then(function (res) {
                    var index = $scope.getProductos.indexOf(producto);
                    $scope.getProductos.splice($scope.getProductos.indexOf(producto), 1);
                    $scope.getProductos.splice(index, 0, producto);
                }, function (err) {
                    console.error(err);
                });

            };
            //elimina productos
            $scope.deleteProductos = function (producto) {
                var id = producto.id;
                var querydeleteLista = "DELETE  FROM productosrk WHERE id =  ?";
                $cordovaSQLite.execute(db, querydeleteLista, [id]);
                //eliminar de la  vista
                $scope.getProductos.splice($scope.getProductos.indexOf(producto), 1);
                //reducir el  precio
                var cantidadReducir = producto.cantidad * producto.precio;
                var total = document.getElementById('valorTotalPagar').value;
                var resultado = total - cantidadReducir;
                //presentar el resultado en la vista
                var totalLista =  parseFloat(Math.round(resultado * 100) / 100).toFixed(2);
                document.getElementById('valorTotalPagar').value = totalLista;
                actualizarLista($stateParams.listaId, totalLista);
            };

             //actualiza precio lista
            actualizarLista = function (id_lista, precio) {
                var update = "UPDATE  listasrk SET precio = ? WHERE ID = ? ";
                $cordovaSQLite.execute(db, update, [precio, id_lista]).then(function (res) {
                    //$scope.getAllListas();
                }, function (err) {
                    console.error(err);
                });
            };

            $scope.getProductos = [];
            /*obtener todos los productos de la lista*/
            $scope.getAllProductos = function () {
                var total = 0.0;
                var slectproductos = [];
                listasProductos = slectproductos;
                var queryProductos = "SELECT *  FROM productosrk WHERE id_lista =  ? ORDER BY ID DESC";
                $cordovaSQLite.execute(db, queryProductos, [$stateParams.listaId]).then(function (res) {
                    if (res.rows.length > 0) {
                        for (var i = 0, max = res.rows.length; i < max; i++) {
                            total = total + res.rows.item(i).precioTotal;
                            slectproductos.push({"id": res.rows.item(i).ID, 
                                                 "nombre": res.rows.item(i).nombre, 
                                                 "codigo": res.rows.item(i).codigo, 
                                                 "precio": res.rows.item(i).precio, 
                                                 "cantidad": res.rows.item(i).cantidad, 
                                                 "ubicacion_id": res.rows.item(i).ubicacion_id, 
                                                 "estado": res.rows.item(i).estado, 
                                                 "imagen": res.rows.item(i).imagen, 
                                                 "comprado": res.rows.item(i).comprado});
                            //presentar el valor de la lista  total en la lista
                            //document.getElementById('valorTotalPagar').value = document.getElementById('valorTotalPagar').value = parseFloat(Math.round(total * 100) / 100).toFixed(2);
                            document.getElementById('valorTotalPagar').value = parseFloat(Math.round(total * 100) / 100).toFixed(2);

                        }
                        $scope.getProductos = slectproductos;
                        listasProductos = slectproductos;

                        actualizarLista($stateParams.listaId, document.getElementById('valorTotalPagar').value)

                    } else {
                        console.log("No results found");
                    }
                }, function (err) {
                    console.error(err);
                });
            };
        })
            /*controlador al crear una lista nueva*/
        .controller('NuevaListaCtrl', function ($scope, $ionicModal, $timeout) {
            // Create the login modal that we will use later
            $ionicModal.fromTemplateUrl('templates/nuevaLista.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
            });
            // Triggered in the login modal to close it
            $scope.closeLogin = function () {
                $scope.modal.hide();
            };
            // abrir modal
            $scope.login = function () {
                $scope.modal.show();
            };
            // cerrar modal
            $scope.doLogin = function () {
                /*actualizar la lista*/
                $scope.getAllListas();
                $timeout(function () {

                    $scope.closeLogin();
                }, 500);
            };
        })

        //controlador escanear
        .controller('escanearCtrl', function ($scope,
                                              $ionicModal,
                                              $timeout,
                                              $http,
                                              $translate,
                                              StorageService) {
            // Create the login modal that we will use later

            $ionicModal.fromTemplateUrl('templates/escanear.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
            });
            // Triggered in the login modal to close it
            $scope.closeLogin = function () {
                $scope.modal.hide();
            };
            // Open the login modal
            $scope.login = function () {
                $scope.modal.show();
            };
            // Perform the login action when the user submits the login form
            $scope.doLogin = function () {
                console.log('Doing login');
                $scope.getAllProductos();
                $timeout(function () {
                    $scope.closeLogin();
                }, 500);
            };
            //escanea producto
            $scope.startScan = function () {
                $scope.resultadoproducto = [];
                cordova.plugins.barcodeScanner.scan(
                        function (result) {
                            //enviar el codigo  de barra al servidor
                            $scope.cargando = true;
                            var bd_super = StorageService.getBd();
                            var url = urlws + "/productos/codigob";
                            if (result.text) {
                                url += "/" + result.text + "/" + bd_super;
                                $http.get(url).success(function (respuesta) {
                                    //respuesta del servidor
                                    $scope.resultadoproducto = respuesta;
                                    $scope.cargando = false;

                                }).error(function (data, status, headers, config) {
                                    $scope.cargando = false;
                                    /*presentar el mensaje de producto no encontrado*/
                                    $translate(['productnofound']).then(function (translations) {
                                        window.plugins.toast.showShortTop(translations.productnofound + " !", function (a) {
                                            console.log('toast success: ' + a)
                                        }, function (b) {
                                            alert('toast error: ' + b)
                                        });
                                    });

                                    $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                                    console.log('-->' + $scope.token);
                                });
                            }

                        },
                        function (error) {
                            $scope.cargando = false;
                            alert("Scanning failed: " + error);
                        }
                );
            };


        })
        //controlador de buscar productos
        .controller('buscarCtrl', function ($scope,
                                            $ionicModal,
                                            $timeout,
                                            $http,
                                            $translate,
                                            StorageService,
                                            $ionicPopup) {

            var result = StorageService.getAll();
            $scope.servicioPromociones = result[0].promocion;

            /*coloear azul cuando aparece campo*/
            $scope.colorear = "calm-border";

            $scope.colorSuccess = function () {
                $scope.colorear = "calm-border";
            }

            $ionicModal.fromTemplateUrl('templates/buscar.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
            });
            // Triggered in the login modal to close it
            $scope.closeLogin = function () {
                $scope.modal.hide();
            };
            // Open the login modal
            $scope.login = function () {
                $scope.modal.show();
            };
            // Perform the login action when the user submits the login form
            $scope.doLogin = function () {
                $scope.getAllProductos();
                $scope.resetValues();
                $timeout(function () {
                    $scope.closeLogin();
                }, 500);
            };

            $scope.chageInputreset = function () {
                $scope.productosEncontrados = [];
            }

            $scope.resetValues = function () {
                $scope.productosEncontrados = [];
                if (document.getElementById('nombreBuscar'))
                    document.getElementById('nombreBuscar').value = "";
            }

            $scope.verDetalles = function(producto) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Detalles!',
                    template: "<div class='item item-divider'>" +
                                    "Nombre:" +
                                  "</div>" +
                                  "<a class='item'>"+
                                    "<h6>" + producto.nom_prod + "</h6>"+
                                  "</a>" +
                                  "<div class='item item-divider'>" +
                                    "Precio:" +
                                  "</div>" +
                                    "<a class='item'>"+
                                        "<h6> $ " + producto.pvp_prod + "</h6>"+
                                    "</a>" +
                                "</div>"
                });

                alertPopup.then(function(res) {
                    console.log('Thank you for not eating my delicious ice cream cone');
                });
            }

            //busca  productos por nombre en la base de datos
            $scope.cargar_productos = function (nombre_buscar) {
                //verficiar la conexion a internet
                //var conexion_internet = false;
                var conexion_internet = true;
                
                /*var networkState = navigator.connection.type;
//
                if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
                    conexion_internet = false;
                } else {
                    conexion_internet = true;
                }*/


                if (conexion_internet) { //verificar si existe conexion a internet
                    if (nombre_buscar) {
                        $scope.cargando = true;
                        var bd_super = StorageService.getBd();
                        var url = urlws + "/productos/getProductos/" + nombre_buscar + "/" + bd_super;
                        $http.get(url).success(function (respuesta) { // envia peticion get
                            console.log(respuesta);
                            if (respuesta != '400') {
                                $scope.productosEncontrados = respuesta;
                                listasProductos = respuesta;
                                //ocultar sppiner
                                $scope.cargando = false;
                            } else {
                                /*presentar un mensaje que del producto no a sido encontrado*/
                                $translate(['productnofound']).then(function (translations) {
                                    window.plugins.toast.showShortTop(translations.productnofound + " !", function (a) {
                                        console.log('toast success: ' + a)
                                    }, function (b) {
                                        alert('toast error: ' + b)
                                    });
                                });
                                $scope.cargando = false;
                            }

                        }).error(function (data, status, headers, config) {
                            $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                            console.log('-->' + $scope.token);
                        });

                    } else {
                        /*cambiar de color al cuadro de texto en caso de no ingresar un producto*/
                        $scope.colorear = "assertive-border";
                        /*ingrese un nombre*/
                        $translate(['in']).then(function (translations) {
                               window.plugins.toast.showShortBottom(translations.in + ". !", function (a) {
                                    console.log('toast success: ' + a)
                                }, function (b) {
                                    alert('toast error: ' + b)
                                });
                            });
                    }
                } else {
                    /*cambiar de color al cuadro de texto en caso de no ingresar un producto*/
                    console.log("no hay conexion a internet")
                }
            }
            /*borrar los productos buscados*/
            $scope.reset_lista_buscados = function () {
                $scope.productosEncontrados = [];
            }

            //seccion de tabs promociones y cargarproductos
            $scope.buscarProductos = true;
            $scope.mostrarPromociones = false;

            $scope.mostrar_produc = function(){
                $scope.buscarProductos =  true
                $scope.mostrarPromociones = false;
            }
            $scope.mostrar_promo = function(){
                $scope.mostrarPromociones = true;
                $scope.buscarProductos =  false;
            }
        })
        .controller('enviarListaCtrl', function ($scope, 
                                                $ionicModal, 
                                                $stateParams, 
                                                $http, 
                                                $translate, 
                                                StorageService,
                                                $cordovaSQLite,
                                                $ionicPopup) {

            $scope.pagarLista = function(lista, listaproductos){
                //establecer datos para cargar
                //shopPaypal.initPaymentUI();

               
                var precio = document.getElementById('valorTotalPagar').value;
                
                var arreglo = StorageService.getUser();
                var usuario = arreglo[0];
                var data = {id_user :  usuario[0].id, productos : metodos.getAllProd(), entrega : 0};
                var bd_super = StorageService.getBd();
                var pagar_entrega = StorageService.pagar_entrega();
                console.log("pagar entrada" , pagar_entrega);
                
                if (metodos.getAllProd().length > 0) {

                    //retirar o entrega
                    $translate(['entregadomic', 'costoadicional', 'cancelartext', 'oktext']).then(function (translations) {
                       var confirmPopup = $ionicPopup.confirm({
                          title: translations.entregadomic,
                          template: translations.costoadicional + ' $ ' + pagar_entrega,
                          cancelText: translations.cancelartext, // String (default: 'Cancel'). The text of the Cancel button.
                          okText: translations.oktext

                       });

                       confirmPopup.then(function(res) {
                          if (res) {
                            var result = parseFloat(precio) + parseFloat(pagar_entrega);
                            var preciopagar = result.toFixed(2);
                            //actualizar la data a enviar al supermercado
                            data.entrega = 1;
                            shopPaypal.comprar(preciopagar, lista, data, $http, bd_super, $cordovaSQLite, $translate);

                          } else {
                            shopPaypal.comprar(precio, lista, data, $http, bd_super, $cordovaSQLite, $translate);
                          }
                        });
                    });
                } else {

                    $translate(['sinproductos']).then(function (translations) {
                        window.plugins.toast.showShortBottom(translations.sinproductos + ". !", function (a) {
                            console.log('toast success: ' + a)
                        }, function (b) {
                            alert('toast error: ' + b)
                        });
                    });
                }
            }
        })
        //controlador detalles de un producto seleccionado
        .controller('prod_detail_Ctrl', function ($scope,
                                                  $stateParams,
                                                  $ionicModal,
                                                  $ionicHistory,
                                                  $timeout,
                                                  $http,
                                                  StorageService) {

            var result = StorageService.getAll();
            $scope.servicioUbicacionProd = result[0].ubicacion;

            //ubicacion de producto general upg
            var conexion = true;
            $scope.existeInternetupg = function () {
                //conecionInternet
                var networkState = navigator.connection.type;

                if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
                    $scope.existeInternetenupg = false;
                } else {
                    $scope.existeInternetenupg = true;
                }

//                $scope.existeInternetenupg = true;
                conexion = $scope.existeInternetenupg;
            }

            //retoceder a una vista anterior
            $scope.myGoBack = function () {
                $ionicHistory.goBack();
            };
            //los campos del formulario desabilidatos por defecto
            $scope.disable = true;
            //habilita campos
            $scope.habilitarCampos = function (productSelect) {
                $scope.disable = productSelect.estado;
            };
            //obtener el producto seleccionado
            $scope.productSelect = metodos.getProductoId($stateParams.productoId);
            /*obtener si existe conexion internet para presentar la imagen*/
            //aumentar la cantidad a comprar
            $scope.aumentarCantidad = function () {
                $scope.productSelect.cantidad++;
            };
            //disminuir la cantidad a comprar
            $scope.disminuirCantidad = function () {
                if ($scope.productSelect.cantidad > 1) {
                    $scope.productSelect.cantidad--;
                }
            };

            //seccion modal de localizacion de productos
            $ionicModal.fromTemplateUrl('templates/locationProduct.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
            });
            // Triggered in the login modal to close it
            $scope.closeLogin = function () {
                $scope.modal.hide();
            };
            // Open the login modal
            $scope.login = function () {
                $scope.modal.show();
            };
            // Perform the login action when the user submits the login form
            $scope.doLogin = function () {
                console.log('Doing login');
                $timeout(function () {
                    $scope.closeLogin();
                }, 500);
            };

            //obtener la ubicacion del producto agregado a la lista desde la bd
            $scope.cargar_Ubicacion = function (productSelect) {
               $scope.existeInternetupg()

                if (conexion) {
                    var bd_super = StorageService.getBd();
                    var url = urlws + "/productos/ubicacion";
                    url += "/" + productSelect.ubicacion_id + "/" + bd_super ;
                    $http.get(url).success(function (respuesta) {

                        $scope.ubicacionPoducto = respuesta;

                        console.log('Resultado' + respuesta);
                    }).error(function (data, status, headers, config) {
                        $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                        console.log('-->' + $scope.token);
                        //presentar un mensaje en la vista
                        $scope.estadoUbicacion = true;
                    });
                }
            };
            //cagar ubicacion en la seccion de solo busqueda
            $scope.cargar_Ubicacion_Buscada = function (productoSeleccionado) {
                $scope.existeInternetupg();

                if (conexion) { //verificar si existe conexion a internet
                    var url = urlws + "/productos/ubicacion";
                    url += "/" + productoSeleccionado.ubicacion_id;
                    $http.get(url).success(function (respuesta) {
                        $scope.ubicacionPoducto = respuesta;

                    }).error(function (data, status, headers, config) {
                        $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                        console.log('-->' + $scope.token);
                        //presentar un mensaje en la vista
                        $scope.estadoUbicacion = true;
                    });
                }
            };
        })
        //controlador seccion promociones     
        .controller('promocionesCtrl', function ($scope,
                                                 $http,
                                                 StorageService,
                                                 $stateParams,
                                                 $cordovaSQLite,
                                                 $translate) {
            //verificar la conexion a internet
            $scope.existeInternetPromocion = function () {
                //conecionInternet
               var networkState = navigator.connection.type;
                if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
                    $scope.conexion_internet_promociones = false;
                } else {
                    $scope.conexion_internet_promociones = true;
                }
                //$scope.conexion_internet_promociones = true;
            }
            $scope.numero_promociones = 0; //en caso de no haber promociones
            //actualiza promociones
            $scope.doRefresh = function () {
                $scope.cargar_promociones();
                $scope.$broadcast('scroll.refreshComplete');
            };
            //obtiene promociones de la bd
            $scope.cargar_promociones = function () {
                var bd_super = StorageService.getBd();
                var url = urlws + "/productos/promocion/" + bd_super;
                $http.get(url).success(function (respuesta) {
                                       console.log(respuesta);
                    //cargar todas las promociones disponibles
                    $scope.promocionesEncontradas = respuesta;
                    //cargar el numero de promociones
                    $scope.numero_promociones = respuesta.length;
                }).error(function (data, status, headers, config) {
                    $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                    console.log('mierda -->' + $scope.token);
                });
            };

            //insertar productos desde una promocion bd movil
            $scope.insertProductosPromocion = function (producto) {
                
                var id_lista = $stateParams.listaId;
                var cantidad = 1;
                var precioTotal = producto.pvp_pago;
                console.log("precio total " , precioTotal);
                var data = {"nombre "   : producto.nom_prod, 
                            "codigo"    : producto.codigo,
                            "preciovp"  : producto.pvp_pago,
                            "id_lista"  : id_lista, 
                            "imagen"    : producto.imagen, 
                            "cantidad " : cantidad, 
                            "precioT"   : precioTotal, 
                            "ubicacion" : producto.ubicacion_id};

                var query = "INSERT INTO  productosrk (nombre, codigo, precio , id_lista, imagen , cantidad , precioTotal , ubicacion_id , estado , comprado) VALUES (? ,?, ?, ? , ? , ?, ? , ? , ? , ?)";
                $cordovaSQLite.execute(db, query, [producto.nom_prod, producto.codigo, producto.pvp_pago, id_lista, producto.imagen, cantidad, precioTotal, producto.ubicacion_id, true, false]).then(function (res) {
                    //para la seccion de busqueda .. en caso de ser agregado que se elimine de la vista
                    $scope.promocionesEncontradas.splice($scope.promocionesEncontradas.indexOf(producto), 1);
                    //presentar un mensaje de producto agregado
                    $translate(['productoadd']).then(function (translations) {
                        window.plugins.toast.showShortBottom(translations.productoadd + ". !", function (a) {
                            console.log('toast success: ' + a)
                        }, function (b) {
                            alert('toast error: ' + b)
                        });
                    });


                }, function (err) {
                    console.error(err);
                });
            };

            $scope.precioFinalPromo = function(promo){
                var resultado = promo.pvp_prod - (promo.pvp_prod * promo.descuento);
                promo.pvp_pago = parseFloat(Math.round(resultado * 100) / 100).toFixed(2);
                //console.log(">> " , promo.pvp_prod - (promo.pvp_prod * promo.descuento));
                return promo.pvp_pago;
            }
            
        });
//metodos para la seccion de productos y listas
var metodos = {
    //obtiene un producto de la vista por un id seleccionado
    getProductoId: function (productoId) {
        for (var i = 0; i < listasProductos.length; i++) {
            if (listasProductos[i].id === parseInt(productoId)) {
                return listasProductos[i];
            }
        }
        return null;
    },
    getSupermercado: function (lista, nombre_bd) {
        for (var i = 0; i < lista.length; i++) {
            if (lista[i].nombre_bd === nombre_bd) {
                return lista[i];
            }
        }
        return null;
    },
    getAllProd: function () {
        return listasProductos;
    },
    //obtiene una lista de la vista por un id seleccionado
    getListaId: function (listaId) {
        for (var i = 0; i < totalListas.length; i++) {
            if (totalListas[i].id === parseInt(listaId)) {
                return totalListas[i];
            }
        }
        return null;
    },
    //elima lista de la vista
    remove: function (lista) {
        totalListas.splice(totalListas.indexOf(lista), 1);
    }
};

