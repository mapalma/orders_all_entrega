(function() {
  'use strict';

  angular
    .module('starter')
    .controller('MapController', MapController);

  /** @ngInject */
  function MapController(MapService, OrderPlaceService, $cordovaGeolocation,$state) {
    var vm = this;
    var map;
    var socket;


    vm.iniciarmap = iniciarmap;
    vm.getDirections = getDirections;
      //vm.entregalati=-2.1709979;
     // vm.entregalongi=-79.92235920000002;

    var posOptions = {timeout: 10000, enableHighAccuracy: false};

      function iniciarmap () {
          var myLatlng = new google.maps.LatLng(vm.coordenadasCliente[0], vm.coordenadasCliente[1]);
          var mapOptions = {
              zoom: 17,
              center: myLatlng
          }
          /*dibujar el mapa en el html*/
          map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
          var marker = new google.maps.Marker({
              position: myLatlng,
              map: map
          });
          /*cargar nombre del supermecado en el puntero del mapa*/
          var infowindow = new google.maps.InfoWindow({content: "Cliente"});
          infowindow.open(map, marker);
      };
      // directions object -- with defaults

      var directionsDisplay = new google.maps.DirectionsRenderer();
      var directionsService = new google.maps.DirectionsService();
      var geocoder = new google.maps.Geocoder();

       // get directions using google maps api
       function getDirections() {
              var request = {
              origin: vm.directions.origin,
              destination: vm.directions.destination,
              travelMode: google.maps.DirectionsTravelMode.DRIVING,

             };
              directionsService.route(request, function (response, status) {


                      directionsDisplay.setDirections(response);
                      directionsDisplay.setMap(map);
                      directionsDisplay.setPanel(document.getElementById('directionsList'));
                      vm.directions.showList = true;

              });
         }

         function loadCoordenadas() {
           MapService.getpostionSelected(function (response) {
             vm.coordenadasCliente = response.split(',');

             vm.directions = {
                 origin: new google.maps.LatLng(vm.latitude, vm.longitude),
                 destination: new google.maps.LatLng(vm.coordenadasCliente[0], vm.coordenadasCliente[1]),
                 showList: false
               }

             iniciarmap();
             getDirections();
           });
         }


         $cordovaGeolocation
         .getCurrentPosition(posOptions)

         .then(function (position) {
            var lat  = position.coords.latitude
            var long = position.coords.longitude
            console.log(lat + ' otraaa  ' + long)
             loadCoordenadas();
         }, function(err) {
            console.log(err)
         });

         var watchOptions = {timeout : 3000, enableHighAccuracy: false};
         var watch = $cordovaGeolocation.watchPosition(watchOptions);

         watch.then(
            null,

            function(err) {
               console.log(err)
            },

            function(position) {
               var lat  = position.coords.latitude
               var long = position.coords.longitude
               console.log(">>>" + lat + '' + long);
               vm.latitude = lat;
               vm.longitude = long;
               resendLocation();
                loadCoordenadas();
            }
         );

         //watch.clearWatch();

               function getUser() {
                 OrderPlaceService.getOrderSelected(function (order) {
                   vm.userSelected = order;
                   console.log("el usuaruooooooooooooooooooooooo" , vm.userSelected);
                 });
               }

               function connetSocket() {
                 console.log("es hora de conectarse con el socket entregador")
                 //var socket = io('http://127.0.0.1:3000');
                 socket = io.connect(urlws);
               }

               function resendLocation() {
                 vm.user = {
                   id : vm.userSelected.id_usuario,
                   nombre : vm.userSelected.nombre_usuario,
                   latitude : vm.latitude,
                   longitude : vm.longitude

                 }
                 console.log(">>>> el usuario seleccionado" , vm.user);
                 socket.emit('location', vm.user);
                 //crear el escuchador del server
                 escuchador();
               }

               function escuchador() {
                 socket.on('foo', function(msg) {
                      console.info(msg);
                  });
               }


              connetSocket();
              getUser();

  }

})();
