(function() {
  'use strict';

  angular
    .module('starter')
    .factory('MapService', MapService);

    function MapService($http) {
        var service = {};
        var postionSelected;

        service.setPosition = function (coordenadas) {
          postionSelected = coordenadas;
        }

        service.getpostionSelected = function (callback) {
          callback(postionSelected);
        }

        return service;
    }
})();
