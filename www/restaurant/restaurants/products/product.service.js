(function() {
  'use strict';

  angular
    .module('starter')
    .factory('ProductService', ProductService);

    function ProductService($http) {
        var service = {};
        var productoSelected;
        var restaurants = 'undefined';

       service.setProduct = function (restaurant) {
          productoSelected = restaurant;
        }

        service.getProductSelected = function (callback) {
          callback(productoSelected);
        }

        return service;
    }
})();
