(function() {
  'use strict';

  angular
    .module('starter')
    .controller('PedidoController', PedidoController)
    .controller('ModalController', ModalController);

  /** @ngInject */
  function PedidoController( ) {
    var vm = this;

  }

  function ModalController($scope, $state, $ionicModal, $timeout, PedidoService, RestaurantService, StorageService) {

    $scope.pedidoTypeName = 'Editar';
    $scope.subtotal = 0;

    $scope.calculateSubTotal = function() {
      $scope.subtotal = 0;
      angular.forEach($scope.pedidos, function(value, key) {
        $scope.subtotal += (value.cantidad * value.valor);
      });
      $scope.calculateTotal();
    }

    $scope.calculateTotal = function () {
      console.log("subtotal " , $scope.subtotal);
      console.log("costo envio " , $scope.restaurant.costoEnvio);
      $scope.total = (parseInt($scope.subtotal) + parseInt($scope.restaurant.costoEnvio));
    }

    $scope.showList = function() {
      console.log("es llamado");
      $scope.pedidoTypeName = $scope.pedidoTypeName === 'Editar' ? 'Mi Lista' : 'Editar';
    }

    function loadPedidos() {
      //get pedidos
      PedidoService.getPedidos(function(response) {
        $scope.pedidos = response;
        $scope.calculateSubTotal();
      });
      //get Restaurant Selected
      RestaurantService.getRestaurantSelected(function(response) {
        $scope.restaurant = response;
      });
    }

    $ionicModal.fromTemplateUrl('restaurant/modal/pedidos.tpl.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.closeModal = function () {
      $scope.modal.hide();
    };

    $scope.openModal = function () {
      loadPedidos();
      $scope.modal.show();
    };

    $scope.$on('modal.hidden', function() {
      console.log('Modal is hidden!');
      $state.reload();
    });

    $scope.doModal = function () {
        $timeout(function () {
          console.log("se cerro");
            $scope.closeModal();
        }, 100);
    };

    //section edition

    $scope.deleteProduct = function(product) {
      $scope.pedidos.splice($scope.pedidos.indexOf(product), 1);
      $scope.calculateSubTotal();
      $scope.removeNumberProduct();
    }

    $scope.removeNumberProduct = function () {
      PedidoService.removeNumberProduct();
    }

    //seccion de pvp_pago

    function getRestaurante() {
      RestaurantService.getRestaurantSelected(function (response) {
        $scope.restaurant = response;
      });
    }

    $scope.saveByProducts = function () {
      var pedido = {
        costo : 100,
        iva : 0.12,
        fecha : new Date(),
        estado : 'PREPARANDO'
      }
      var usuario = StorageService.getUser();
      var user = usuario[0];
      var _nombre = user[0].nombres + " " + user[0].apellidos;

      var _usuario = {
        nombre : _nombre,
        direccion : user[0].ubicacion,
        telefono : user[0].telefono
      }

      var data = {
        pedidos : pedido,
        usuario : _usuario,
        productos : $scope.pedidos,
        restaurant : $scope.restaurant,
        id_usuario : user[0].id,
        coordenadas : '-3.999761928618461, -79.21676740050316',
        fecha_pedido : new Date()
      };

      console.log("el usuario es " , data);
      PedidoService.sendOrders(data, function(response) {
        console.log("el estado es ", response);
          if (response.data === 'OK') {
            //eliminar la lista del servicio
          }
      });

    };

    getRestaurante();

  }

})();
