(function() {
  'use strict';

  angular
    .module('starter')
    .factory('PedidoService', PedidoService);

    function PedidoService($http, $ionicModal) {
        var service = {};
        var productoSelected;
        var pedidos = [];
        var numberProducts = 0;


        service.addProduct = function(product) {
          pedidos.push(product);
          numberProducts ++;
        }

        service.removeNumberProduct = function() {
          numberProducts --;
        }

        service.getPedidos = function(callback) {
          callback(pedidos);
        }

        service.getNumbersProducts = function (callback) {
          callback(numberProducts);
        }

        service.destroyOrder = function () {
          pedidos = [];
          numberProducts = 0;
        }

        service.sendOrders = function(data, callback) {
          var url = urlws + "/saveOrders";
          console.log("se va a enviar " , data);
          $http.post(url, data)
            .then(function(res) {
              callback(res);
            }, function(err){
              callback(err);
            });
        }

        return service;

    }
})();
